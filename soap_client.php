<?php

require_once 'XML/OrderSaleDelivery.php';
require_once 'XML/MySoapClient.php';
require_once 'XML/GoodsOnOrder.php';
require_once 'XML/GoodsOnOrderComplete.php';
require_once 'XML/OrderServicesExport.php';


$osd = new OrderSaleDelivery('MKxdrtg', 'Rdop789');
$osd->setDocumentInfo("Cash", "DepoHDS", "EProton", "20064455", "Poznamka", 1);
$osd->setAddressId("B5D1BBFA-9040-4F2D-B38D-5CA0CCFEA259");
$osd->setInvoiceAddress("Jmeno", "Ulice", "Mesto", "PSC", "214457884", "CZ214457884", "123456789", "123456789", "email@email.cz");
$osd->setDeliveryAddress("Jmeno", "Ulice", "Mesto", "PSC", "123456789", "123456789", "email@email.cz");
$osd->addItem("2", "1149", "5000174621167");
$osd->addItem("3", "639", "8591184001188");
$osd->addFreightageType('HDSBALIK');

var_dump($osd->getXml()->saveXML());

$goo = new GoodsOnOrder('MKxdrtg', 'Rdop789');
$goo->setAddressId("B5D1BBFA-9040-4F2D-B38D-5CA0CCFEA259");


$gooc = new GoodsOnOrderComplete('MKxdrtg', 'Rdop789');
$gooc->setAddressId("B5D1BBFA-9040-4F2D-B38D-5CA0CCFEA259");


$ose = new OrderServicesExport('MKxdrtg', 'Rdop789');
var_dump($ose->getXml()->saveXML());


$client = new MySoapClient('http://voistest.hptronic.cz/Reseller.asmx?WSDL', array(
    'soap_version'=>SOAP_1_2,
    'exceptions'=>true,
    'trace'=>1,
));

//OrderSaleDelivery
    $result = $client->__anotherRequest("OrderSaleDelivery", $osd->getXml()->saveXML());
    $items = $osd->getElement($result, "Item");

    var_dump($items);



//GoodsOnOrder
    $goo->setCondition("ALL", $items[0]['attributes']['OrderNumber'], $items[0]['attributes']['IDTrans']);
    $result = $client->__anotherRequest("GoodsOnOrder", $goo->getXml()->saveXML());
    echo $result; 

    //předefinování atributů v uzlu Condition
    $goo->setCondition("ALL", $items[0]['attributes']['OrderNumber'], $items[1]['attributes']['IDTrans']);
    $result = $client->__anotherRequest("GoodsOnOrder", $goo->getXml()->saveXML());
    echo $result;


//GoodsOnOrderComplete - tato funkce je momentálně zrušená
    $gooc->setCondition("ALL", $items[0]['attributes']['OrderNumber'], $items[0]['attributes']['IDTrans']);
    $result = $client->__anotherRequest("GoodsOnOrderComplete", $gooc->getXml()->saveXML());
    echo $result;


//OrderServicesExport
    $result = $client->__anotherRequest('OrderServicesExport', $ose->getXml()->saveXml());
    echo $result;


?>

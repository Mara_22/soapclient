<?php
/**
 * Created by PhpStorm.
 * User: Marek Hučík
 * Date: 2.11.14
 * Time: 11:30
 */

require_once 'MyXml.php';

class OrderSaleDelivery extends MyXml {

    /**
     * @var DOMElement
     */
    private $document;

    /**
     * @var DOMElement
     */
    private $partner;

    /**
     * @var DOMElement
     */
    private $items;

    /**
     * @var array Všechny možnosti platby faktury
     */
    private $payTypes;

    /**
     * @var array Všechny možnosti dopravy
     */
    private $transportTypes;

    /**
     * @var array Všechny možnosti dopravného
     */
    private $freightageTypes;

    public function __construct($username, $password) {
        parent::__construct($username, $password);
        $this->setPayTypes();
        $this->setFreightageTypes();
        $this->setTransportTypes();
        $this->createPartner();
        $this->createItems();
        $this->createHeader();
        $this->createBody();
    }

    private function createBody() {
        $body = $this->getXml()->createElement("soap12:Body");
        $osd = $this->getXml()->createElement("OrderSaleDelivery");
        $osd->setAttribute("xmlns", "http://HPTronic.cz/");
        $request = $this->getXml()->createElement("request");
        $this->document = $this->getXml()->createElement("Document");
        $this->document->setAttribute("xmlns", "http://www.cgcc.cz/schemas/Reseller/OrderSaleDelivery.xsd");
        $this->document->appendChild($this->partner);
        $this->document->appendChild($this->items);
        $request->appendChild($this->document);
        $osd->appendChild($request);
        $body->appendChild($osd);
        $this->getXml()->firstChild->appendChild($body);
    }

    private function createPartner() {
        $this->partner = $this->getXml()->createElement("Partner");
    }

    private function createItems() {
        $this->items = $this->getXml()->createElement("Items");
    }

    /**
     * @param $payType způsob úhrady za objednané zboží (povinná položka)
     * @param $transportType způsob přepravy zboží k zákazníkovi
     * @param $IDOrder identifikace objednávky partnera
     * @param $shopName název elektronického obchodu partnera
     * @param $note poznámka
     * @param int $test pokud je vyplněno 1, provedou se všechny operace, ale rezervace nezůstane uložená, aplikace vrátí odpoveď stejně jako když se provádí  rezervace bez Test=“1“
     */
    public function setDocumentInfo($payType, $transportType, $shopName, $IDOrder = NULL, $note = NULL, $test = 0) {
        if(in_array($payType, $this->payTypes))
            $this->document->setAttribute("PayType", $payType);
        else
            throw new BadPayTypeException("'$payType' není standardním typem platby");

        if(in_array($transportType, $this->transportTypes))
            $this->document->setAttribute("TransportType", $transportType);
        else
            throw new BadtransportTypeException("'$transportType' není standardním typem přepravy");

        if($IDOrder != NULL)
            $this->document->setAttribute("IDOrder", $IDOrder);

        $this->document->setAttribute("ShopName", $shopName);

        if($note != NULL)
            $this->document->setAttribute("Note", $note);

        if($test == 1 || $test == 0)
            $this->document->setAttribute("Test", $test);
        else
            throw new BadTestValueException("Hodnota '$test' u položky 'test' není povolena");
    }

    public function setAddressId($id) {
        $address = $this->getXml()->createElement("Address");
        $address->setAttribute("ID", $id);
        $this->partner->appendChild($address);
    }

    public function setInvoiceAddress($name, $street, $city, $zip, $numid = NULL, $vatid = NULL, $phone = NULL, $mobile = NULL, $email = NULL) {
        $invoiceAddress = $this->getXml()->createElement("InvoiceAddress");
        $this->setAddress($invoiceAddress, $name, $street, $city, $zip, $phone, $mobile, $email);

        if($numid != NULL)
            $invoiceAddress->setAttribute("NumID", $numid);
        if($vatid != NULL)
            $invoiceAddress->setAttribute("VATID", $vatid);

        $this->partner->appendChild($invoiceAddress);
    }

    public function setDeliveryAddress($name, $street, $city, $zip, $phone = NULL, $mobile = NULL, $email = NULL) {
        $deliveryAddress = $this->getXml()->createElement("DeliveryAddress");
        $this->setAddress($deliveryAddress, $name, $street, $city, $zip, $phone, $mobile, $email);
        $this->partner->appendChild($deliveryAddress);
    }

    public function addItem($amount, $unitVatPrice, $ean = NULL, $id = NULL, $match = NULL) {
        if($id == NULL && $match == NULL && $ean == NULL)
            throw new ItemIdentificatorNotDefined("ID, MATCH nebo EAN musí být vyplněné u zboží");

        $item = $this->getXml()->createElement("Item");

        if($ean != NULL)
            $item->setAttribute("EAN", $ean);

        if($id != NULL)
            $item->setAttribute("ID", $id);

        if($match != NULL)
            $item->setAttribute("Match", $match);

        $item->setAttribute("Amount", $amount);
        $item->setAttribute("UnitVatPrice", $unitVatPrice);

        $this->items->appendChild($item);
    }

    /**
     * @param $address DOMElement
     * @param $name String
     * @param $street String
     * @param $city String
     * @param $zip String PSČ
     * @param $phone
     * @param $mobile
     * @param $email
     */
    private function setAddress($address, $name, $street, $city, $zip, $phone = NULL, $mobile = NULL, $email = NULL) {
        $address->setAttribute("Name", $name);
        $address->setAttribute("Street", $street);
        $address->setAttribute("City", $city);
        $address->setAttribute("ZIP", $zip);
        if($phone != NULL)
            $address->setAttribute("Phone", $phone);
        if($mobile != NULL)
            $address->setAttribute("Mobile", $mobile);
        if($email != NULL)
            $address->setAttribute("Email", $email);
    }

    private function setPayTypes() {
        $this->payTypes = array(
            'Cash',         //hotove
            'Bank',         //prevodem
            'Card'.         //platebni karta
            'Prepayment',   //zaloha
            'Triangl',      //Triangl
            'Cetelem'       //Cetelem
        );
    }

    private function setTransportTypes() {
        $this->transportTypes = array(
            'DepoHDS',      //komfortní doručení
            'PostHDS',      //přeprava poštou
            'Personal',     //Osobní odběr TY
            'PersonalZL',   //Osobní odběr ZL
            'PersonalPL',   //Osobní odběr PL
            'PersonalPR',   //Osobní odběr PR
            'PersonalBR',   //Osobní odběr BR
            'PersonalOV',   //Osobní odběr OV
            'HDSPackage',   //DPD classic Private
            'BASICHDS'      //TOPTRANS doručení
        );
    }

    public function addFreightageType($code) {
        if(!$this->freightageTypes)
            $this->setFreightageTypes();
        if(!array_key_exists($code, $this->freightageTypes))
            throw new BadFreightageTypeException("Typ dopravného '$code' není definován");

        $item = $this->getXml()->createElement("Item");
        $ft   = $this->freightageTypes[$code];

        foreach($ft as $key => $value) {
            $item->setAttribute($key, $value);
        }

        $this->document->appendChild($item);
    }

    private function setFreightageTypes() {
        $this->freightageTypes = array(
            'HDSODVOZ' => array(
                //'IDItem'      => '43678',
                'Match'    => 'HDSODVOZ',
                'Amount'    => "1",
                //'AName'       => 'HDS - odvoz vysloužilého výrobku',
                'Price'       => '0.826',
                //'Description' => 'Starý elektrospotřebič od Vás odvezeme do sběrného střediska a necháme jej ekologicky zlikvidovat. Podmínkou je kompletní a nepoškozené elektrozařízení bez obalu'
            ),
            'HDSODVOZOBALU' => array(
                //'IDItem'        => '55736',
                'Match'         => 'HDSODVOZOBALU',
                'Amount'        => "1",
                //'AName'         => 'HDS - odvoz obalu',
                'Price'         => '41.322',
                //'Description'   => 'Zbavíme Vás všech přepravních obalů. Veškeré kartony, výplně a jiné obalové materiály od Vás odvezeme a necháme je ekologicky zlikvidovat.'
            ),
            'HDSSOBOTA' => array(
                //'IDItem'      => '86793',
                'Match'    => 'HDSSOBOTA',
                'Amount'    => "1",
                //'AName'       => 'HDS - sobotní závoz',
                'Price'       => '247.107',
                //'Description' => 'Objednané zboží Vám doručíme i během sobotního dopoledne. Doručení probíhá od 8 – 16 hodiny. Přesný termín po dohodě s operátorem',
            ),
            'HDSPO17TE' => array(
                //'IDItem'      => '87015',
                'Match'    => 'HDSPO17TE',
                'Amount'    => "1",
                //'AName'       => 'HDS - večerní doručení po 17-té hodině',
                'Price'       => '164.463',
                //'Description' => 'Objednané zboží Vám doručíme i ve večerních hodinách. Doručení probíhá Po-Pá od 17 – 20 hodiny. Přesný termín po dohodě s operátorem.',
            ),
            'HDSPATRO' => array(
                //'IDItem'      => '90765',
                'Match'    => 'HDSPATRO',
                'Amount'    => "1",
                //'AName'       => 'HDS - výpomoc s výnosem do patra',
                'Price'       => '0.826',
                //'Description' => 'Řidič Vám pomůže s transportem výrobku na určené místo. Nyní Vám tuto službu nabízíme za pouhou 1,- / patro. Pro komfortnější využití našich služeb prosíme o vyplnění počtu pater',
            ),
            'HDSDOPRAVNE1' => array(
                //'IDItem'      => '150997',
                'Match'    => 'HDSDOPRAVNE1',
                'Amount'    => "1",
                //'AName'       => 'Dopravné - Komfortní doručení HDS',
                'Price'       => '329.752',
                //'Description' => 'Dopravné - Komfortní doručení HDS',
            ),
            'HDSDOPRAVNE2' => array(
                //'IDItem'      => '150998',
                'Match'    => 'HDSDOPRAVNE2',
                'Amount'    => "1",
                //'AName'       => 'Dopravné - Komfortní doručení HDS',
                'Price'       => '247.107',
                //'Description' => 'Dopravné - Komfortní doručení HDS',
            ),
            'HDSDOPRAVNECP' => array(
                //'IDItem'      => '150999',
                'Match'    => 'HDSDOPRAVNECP',
                'Amount'    => "1",
                //'AName'       => 'Dopravné - Balík do ruky České pošty (poštovné, balné)',
                'Price'       => '106.612',
                //'Description' => 'Obchodní balík České pošty (poštovné, balné)',
            ),
            'HDSOODBER' => array(
                //'IDItem'      => '162811',
                'Match'    => 'HDSOODBER',
                'Amount'    => "1",
                //'AName'       => 'HDS - manipulační poplatek osobní odběr',
                'Price'       => '40.496',
                //'Description' => 'Manipulační poplatek osobní odběr HDS',
            ),
            'HDSDOPRAVNETT1' => array(
                //'IDItem'      => '178234',
                'Match'    => 'HDSDOPRAVNETT1',
                'Amount'    => "1",
                //'AName'       => ' Dopravné – TOPTRANS ',
                'Price'       => '329.752',
                //'Description' => ' Dopravné – TOPTRANS ',
            ),
            'HDSDOPRAVNETT2' => array(
                //'IDItem'      => '178236',
                'Match'    => 'HDSDOPRAVNETT2',
                'Amount'    => "1",
                //'AName'       => ' Dopravné – TOPTRANS ',
                'Price'       => '247.107',
                //'Description' => ' Dopravné – TOPTRANS ',
            ),
            'HDSBALIK' => array(
                //'IDItem'      => '210883',
                'Match'    => 'HDSBALIK',
                'Amount'    => "1",
                //'AName'       => ' Dopravné – BALÍK ',
                'Price'       => '106.612',
                //'Description' => ' Dopravné – BALÍK ',
            ),
        );
    }
}

class BadPayTypeException extends Exception {}
class BadtransportTypeException extends Exception {}
class BadTestValueException extends Exception {}
class ItemIdentificatorNotDefined extends Exception {}
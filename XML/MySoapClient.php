<?php
/**
 * Created by PhpStorm.
 * User: Marek Hučík
 * Date: 5.11.14
 * Time: 2:03
 */

class MySoapClient extends SoapClient {

    public function __doRequest($request, $location, $action, $version) {
        $result = parent::__doRequest($request, $location, $action, $version);
        return $result;
    }

    function __anotherRequest($call, $params) {
        $location = 'http://voistest.hptronic.cz/Reseller.asmx';
        $action = 'http://HPTronic.cz/'.$call;
        $request = $params;
        $result = $this->__doRequest($request, $location, $action, '2');
        return $result;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Marek Hučík
 * Date: 7.11.14
 * Time: 0:01
 */

require_once 'MyXml.php';

class GoodsOnOrder extends MyXml {

    /**
     * @var DOMElement
     */
    private $partner;

    /**
     * @var DOMElement
     */
    private $document;

    /**
     * @var array Všechny atributy
     */
    private $attributes;

    public function __construct($username, $password) {
        parent::__construct($username, $password);
        $this->setAttributes();
        $this->createPartner();
        $this->createHeader();
        $this->createBody();
    }

    private function createPartner() {
        $this->partner = $this->getXml()->createElement("Partner");
    }


    private function createBody() {
        $body = $this->getXml()->createElement("soap12:Body");
        $goo = $this->getXml()->createElement("GoodsOnOrder");
        $goo->setAttribute("xmlns", "http://HPTronic.cz/");
        $request = $this->getXml()->createElement("request");
        $this->document = $this->getXml()->createElement("Document");
        $this->document->setAttribute("xmlns", "http://www.cgcc.cz/schemas/Reseller/GoodsOnOrder.xsd");
        $this->document->appendChild($this->partner);
        $request->appendChild($this->document);
        $goo->appendChild($request);
        $body->appendChild($goo);
        $this->getXml()->firstChild->appendChild($body);
    }

    public function setAddressId($id) {
        $address = $this->getXml()->createElement("Address");
        $address->setAttribute("ID", $id);
        $this->partner->appendChild($address);
    }

    public function setCondition($type, $idOrder, $idTrans) {
        $condition = $this->document->getElementsByTagName("Condition")->item(0);

        if(!$condition)
            $condition = $this->getXml()->createElement("Condition");

        if(in_array($type, $this->attributes))
            $condition->setAttribute("Type", $type);
        else
            throw new BadTypeException("Typ objednávky '$type' nebyl rozpoznán.'");

        $condition->setAttribute("IDOrder", $idOrder);
        $condition->setAttribute("IDTrans", $idTrans);

        $this->document->appendChild($condition);

    }

    private function setAttributes() {
        $this->attributes = array(
            'ALL',//všechny
            'OS',//objednáno skladem
            'ON',//objednáno není skladem
            'OP',//s objednávkou přepravy
            'EX',//expedováno(jen s konkrétními IDTrans(y)
        );
    }
}

class BadTypeException extends Exception {}
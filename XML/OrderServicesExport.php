<?php
/**
 * Created by PhpStorm.
 * User: Marek Hučík
 * Date: 7.11.14
 * Time: 1:02
 */

require_once 'MyXml.php';

class OrderServicesExport extends MyXml {


    public function __construct($username, $password) {
        parent::__construct($username, $password);
        $this->createHeader();
        $this->createBody();
    }

    private function createBody() {
        $body = $this->getXml()->createElement("soap12:Body");
        $ose = $this->getXml()->createElement("OrderServicesExport");
        $ose->setAttribute("xmlns", "http://HPTronic.cz/");
        $body->appendChild($ose);
        $this->getXml()->firstChild->appendChild($body);
    }
        

} 
<?php
/**
 * Created by PhpStorm.
 * User: Marek Hučík
 * Date: 2.11.14
 * Time: 11:32
 */

class MyXml {

    /**
     * @var String uživatelské jméno
     */
    private $username;

    /**
     * @var String heslo k účtu
     */
    private $password;

    /**
     * @var DOMDocument
     */
    private $xml;

    public function __construct($username, $password) {
        $this->username = $username;
        $this->password = $password;
        $this->xml = new DOMDocument('1.0', 'utf-8');
        $root = $this->xml->createElement('soap12:Envelope');
        $root->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        $root->setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
        $root->setAttribute("xmlns:soap12", "http://www.w3.org/2003/05/soap-envelope");
        $this->xml->appendChild($root);
    }

    /**
     * @return DOMElement
     */
    protected function createHeader() {
        $username = $this->xml->createElement('UserName', $this->username);
        $password = $this->xml->createElement('Password', $this->password);
        $header = $this->xml->createElement('soap12:Header');
        $authHeader = $this->xml->createElement('AuthHeader');
        $authHeader->appendChild($username);
        $authHeader->appendChild($password);
        $authHeader->setAttribute("xmlns", "http://HPTronic.cz/");
        $header->appendChild($authHeader);

        $root = $this->xml->firstChild;
        $root->appendChild($header);
    }

    /**
     * @return DOMDocument
     */
    public function getXml() {
        return $this->xml;
    }

    /**
     * @param $xml_response xml
     * @param $element Název uzlu
     * @return array
     */
    public function getElement($xml_response, $element) {
        $return = array();
        $doc = new DOMDocument('1.0', 'utf-8');
        $doc->loadXML($xml_response);
        $elements = $doc->getElementsByTagName($element);
        foreach($elements as $element) {
            $item = array();
            $item["value"] = $element->nodeValue;
            $attributes = array();
            foreach($element->attributes as $att) {
                $attributes[$att->nodeName] = $att->nodeValue;
            }
            $item['attributes'] = $attributes;
            $return[] = $item;
        }
        return $return;
    }
} 
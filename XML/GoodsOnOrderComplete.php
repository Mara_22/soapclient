<?php
/**
 * Created by PhpStorm.
 * User: Marek Hučík
 * Date: 7.11.14
 * Time: 0:54
 */

require_once 'MyXml.php';

class GoodsOnOrderComplete extends MyXml{
    /**
     * @var DOMElement
     */
    private $partner;

    /**
     * @var DOMElement
     */
    private $document;

    public function __construct($username, $password) {
        parent::__construct($username, $password);
        $this->createPartner();
        $this->createHeader();
        $this->createBody();
    }

    private function createPartner() {
        $this->partner = $this->getXml()->createElement("Partner");
    }


    private function createBody() {
        $body = $this->getXml()->createElement("soap12:Body");
        $goo = $this->getXml()->createElement("GoodsOnOrderComplete");
        $goo->setAttribute("xmlns", "http://HPTronic.cz/");
        $request = $this->getXml()->createElement("request");
        $this->document = $this->getXml()->createElement("Document");
        $this->document->setAttribute("xmlns", "http://www.cgcc.cz/schemas/Reseller/GoodsOnOrderComplete.xsd");
        $this->document->appendChild($this->partner);
        $request->appendChild($this->document);
        $goo->appendChild($request);
        $body->appendChild($goo);
        $this->getXml()->firstChild->appendChild($body);
    }

    public function setAddressId($id) {
        $address = $this->getXml()->createElement("Address");
        $address->setAttribute("ID", $id);
        $this->partner->appendChild($address);
    }

    public function setCondition($idOrder, $idTrans) {
        $condition = $this->document->getElementsByTagName("Condition")->item(0);

        if(!$condition)
            $condition = $this->getXml()->createElement("Condition");

        $condition->setAttribute("IDOrder", $idOrder);
        $condition->setAttribute("IDTrans", $idTrans);

        $this->document->appendChild($condition);

    }
} 